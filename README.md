Fun with Concurrency
====================

Source code for [my blog post on concurrency in Golang](https://alexsears.com/2019/10/fun-with-concurrency-in-golang/).

## How to run

```shell script
$ go run .
```
## Example requests

These requests use [`HTTPie`](https://httpie.org/).

```shell script
$ http :7000
HTTP/1.1 200 OK
Content-Length: 13
Content-Type: text/plain; charset=utf-8
Date: Sat, 19 Oct 2019 22:34:11 GMT

Hello, world!

$ http :8000 name==Alex
HTTP/1.1 200 OK
Content-Length: 12
Content-Type: text/plain; charset=utf-8
Date: Sat, 19 Oct 2019 22:34:16 GMT

Hello, Alex!

$ http POST :9000 name=Alex job="Software Engineer" coffee=please
HTTP/1.1 200 OK
Content-Length: 64
Content-Type: text/plain; charset=utf-8
Date: Sat, 19 Oct 2019 22:34:20 GMT

{
    "coffee": "please",
    "job": "Software Engineer",
    "name": "Alex"
}
```
